﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CircleDemo
{
    /// <summary>
    /// Interaction logic for VSMExample.xaml
    /// </summary>
    public partial class VSMExample : Window
    {
        public VSMExample()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "State1", false);
        }
    }
}
